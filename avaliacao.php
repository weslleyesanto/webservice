<?php include_once('include\config.php');


if(isset($_GET) and count($_GET) > 0){
  //$retorno_viewUserRatings = array();

  $pagina_atual = "";

  if(isset($_GET['param']) AND $_GET['param'] == 'q'){


    if(isset($_GET['productId']) AND $_GET['productId'] != '' AND is_numeric($_GET['productId'])){

      $dados = array('productId' => $_GET['productId']);

      $retorno_viewUserRatings = $buscape->viewUserRatings($dados, true);

      /*echo('<pre>');
        print_r($retorno_viewUserRatings); 
        echo('</pre>');
        die();*/
      if($retorno_viewUserRatings->details->status == 'fail'){
        echo('<pre>');
        print_r($retorno_viewUserRatings); 
        echo('</pre>');
        die();
      }
  }//FINAL $_GET['productId'];
  else{
    header('Location: index.php');
  }
}else{
  header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Avaliação</title>
  <?php include_once('include\css.php'); ?>
</head>

<body>

  <div id="wrapper">

    <?php include_once('include\menu.php'); ?>

    <div id="page-wrapper">


      <div class="container-fluid">

        <?php include_once('include\filtro.php'); ?>

        <h1><?=$retorno_viewUserRatings->product->productName?></h1>
        <div id="alert" style="display:none;"> </div>
        
        <div class="product-details">

          <img src="<?=$retorno_viewUserRatings->product->thumbnail['url']?>" title="" alt=""/>
          <?php 

          if(isset($retorno_viewUserRatings->ratingsUsers) AND count($retorno_viewUserRatings->ratingsUsers) > 0) { ?>
          
      <h2><?=$retorno_viewUserRatings->ratingsUsers['recommendedPorcetagem']?>% recomendam esse produto!</h2>


          <?php
          foreach ($retorno_viewUserRatings->ratingsUsers->resumeRatings as $key => $value): 
            $nm_item = $value->item;
          $status = $value->positive;
          $qtd = $value->quantity;
          if($status == 'true'){
            ?>

            <h3><?=$nm_item ?> - <?=$qtd?></h3>

            <?php 
          }
          endforeach; //END FOREACH $especificação item
          //endforeach; //END FOREACH $retorno_viewUserRatings->product
          ?>
          
          <?php }else{ ?>
          <li> Nenhuma Avaliação encontrada!</li>
          <?php }?>

        </div>

      </div>
    </div>
    <!-- /.container-fluid -->

  </div>
  <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/filtro.js"></script>
</body>

</html>
<?php
}else{
  header('Location: index.php');
}
?>