var filtro = (function() {
	var F = {};

	F.setDHTML = function() {
		
		$('#form_buscar').submit( function(e){
			e.preventDefault();
		});

		$("#priceMin").maskMoney({symbol:"R$",decimal:",",thousands:"."});
		$("#priceMax").maskMoney({symbol:"R$",decimal:",",thousands:"."});

		$('#btnBuscar').on('click', function(){
			F.buscar();
		});

	};

	F.buscar = function() {
		//console.log('buscando...'); 
		var categoryId = $('#categoryId').val();
		var priceMin = script.converteMoedaFloat($('#priceMin').val());
		var priceMax = script.converteMoedaFloat($('#priceMax').val());
		
		/*console.log(priceMax + ' <= '+ priceMin);
		console.log(priceMax <= priceMin);
		return false;*/

		var error = false;

		if(categoryId == ''){
			var msg = "Selecione uma categoria!";
			error = true;
		}

		if(priceMax != ''){
			if(priceMax <= priceMin){
				var msg = "Valor Máximo deve ser MAIOR que preço mínimo!";
				error = true;
			}
		}

		if(!error){
			var keyword = $('#keyword').val();
			
			var sort = $('#sort').val();
			var page = 1;
			var url = 'resultado.php?categoryId='+categoryId+'&keyword='+keyword+'&priceMin='+priceMin+'&priceMax='+priceMax+'&sort='+sort+'&param=q&page='+page; 	
			window.location.href = url; 
		}else{
			script.alertError(msg);
		}
	}

	$(function() {
		F.setDHTML();
	});

	return F;
})();