<?php include_once('include\config.php');


if(isset($_GET) and count($_GET) > 0){
	$retorno_find_ofertas = array();

	$pagina_atual = "";

	if(isset($_GET['param']) AND $_GET['param'] == 'q'){

		$retorno_validacao = validar_parametros($_GET);

		$retorno_validacao['priceMin'] = $retorno_validacao['priceMin'] != '' ? $_GET['priceMin'] : '';
		$retorno_validacao['priceMax'] = $retorno_validacao['priceMax'] != '' ? $_GET['priceMax'] : '';

		$retorno_find_ofertas = $buscape->findProductList($retorno_validacao, true);

		/*echo('<pre>');
			print_r($retorno_find_ofertas);	
			die();*/

		if($retorno_find_ofertas->details->status == 'fail'){
			echo('<pre>');
			print_r($retorno_find_ofertas);	
			die();
		}else{

			$_GET['priceMin'] = $retorno_validacao['priceMin'] != '' ? number_format($retorno_validacao['priceMin'], '2', '.', '') : '';
			$_GET['priceMax'] = $retorno_validacao['priceMax'] != '' ? number_format($retorno_validacao['priceMax'], '2', '.', '') : '';

			$pagina_atual = $retorno_find_ofertas['page'];
			$total_paginas = $retorno_find_ofertas['totalPages'];

			$url = 'resultado.php?categoryId='.$_GET['categoryId'].'&keyword='.$_GET['keyword'].'&priceMin='.$_GET['priceMin'].'&priceMax='.$_GET['priceMax'].'&sort='.$_GET['sort'].'&param=q&';
		}
	}else{
		header('Location: index.php');
	}
	?>
	<!DOCTYPE html>
	<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Resultado Filtro</title>
		<?php include_once('include\css.php'); ?>
	</head>

	<body>

		<div id="wrapper">

			<?php include_once('include\menu.php'); ?>

			<div id="page-wrapper">


				<div class="container-fluid">

					<?php include_once('include\filtro.php'); ?>

					<h1>Resultado filtro ofertas</h1>
					<div id="alert" style="display:none;"> </div>
					<hr/>
					<h2>Total de produtos encontrados: <?= $retorno_find_ofertas['totalResultsAvailable']?></h2>
					<!-- <h2>Total de vendedores: <?= $retorno_find_ofertas['totalResultsSellers']?></h2> -->
					<hr/>
					<div id="">

						<?php  if(isset($retorno_find_ofertas->product) AND count($retorno_find_ofertas->product) > 0) { ?>

						<ul class="bp-product-list">
							<?php 
        #INICIO CATEGORIA
							$categoria_id = $retorno_find_ofertas->category['id'];
							$nome_categoria = $retorno_find_ofertas->category->name;
    	#FINAL CATEGORIA
							?>
							<li><?= paginacao($total_paginas, $url); ?></li>
							<?php
							foreach ($retorno_find_ofertas->product as $key => $value): 


    	#INICIO OFFER
								$produto_id = $value['id'];
							$nome_oferta = $value->productName;
							$nome_curto_oferta = $value->productShortName;
							$link = $value->links->link['url'];
							$link_img_oferta = $value->thumbnail['url'];
							$preco = number_format((float)$value->priceMin, 2, ',', '.');

    	## INICO SELLER	
							$id_saller = $value->seller['id'];
							$nome_seller = $value->seller->sellerName;
							$link_img_seller = $value->seller->thumbnail['url'];
							//$link_seller = $value->seller->links->link['url'];
    	### INICIO RATING

    	#### INICIO userAverageRating
							$count_coments_loja = $value->rating->userAverageRating->rating;
    	#### FINAL userAverageRating ####
    	### FINAL RATING ###
    	## FINAL SELLER	##
    	#FINAL OFFER#

							?>
							<li data-id="<?=$produto_id?>" log-ofr-attribute="log-ofr-attribute" class="product track_checkout_container offer ocb-zup-lomadee" log_id="<?=$produto_id?>">
								<div class="image">
									<a data-preco="<?=$preco?>" title="<?=$nome_oferta?>" class="product-image e7Click track_checkout"   href="#">
										<img class="product-image bp_prevent_error resize100" alt="<?=$nome_oferta?>" width="100" src="<?=$link_img_oferta?>" style="visibility: visible;">
									</a>
								</div>
								<div class="description">
									<a data-preco="<?=$preco?>"   class="e7Click track_checkout" href="#"><?=$nome_oferta?></a>

									<a class="offer-page" title="Mais informações" href="#">(+ info)</a>
								</div>
								<div class="details">
									<div class="store-info">
										<ul>
											<li class="">
												<a href="avaliacao.php?param=q&productId=<?=$produto_id?>">
													<span class="ratings"><?=$count_coments_loja?> nota média</span>
												</a>
												<div data-idebit="645" class="ebit-more-info loading">
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="content-holder">
									<div class="single-price">
										<strong class="title">Preço:</strong>
										<strong class="title-por hide">Por:</strong>
										<a data-preco="<?=$preco?>" href="#"   class="price e7Click track_checkout ">
											<span class="value">R$ <?= $preco ?></span>
										</a>
									</div>

								</div>

							</li>
						<?php endforeach; ?>
						<li>
							<?= paginacao($total_paginas, $url); ?>
						</li>
						<?php }else{ ?>
						<li> Nenhuma oferta encontrada!</li>
						<?php }?>

					</ul>

				</div>
			</div>
			<!-- /.container-fluid -->

		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<?php include_once('include\js.php'); ?>
	<script src="assets/js/filtro.js"></script>
</body>

</html>
<?php
}else{
	header('Location: index.php');
}
?>