<?php
//Arquivo criado para criar funções que seram usadas dentro do sistema

/*################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################*/

function replace_vazio($string){
	return str_replace(" ", "-", strtolower($string));
}

function replace_virgula_ponto($string){
	
	$string = str_replace(".", "", $string);
	$string = str_replace(",", ".", $string);
	return $string;
}

//################ INICIO FORMATAR NÚMERO TELEFONE

function formatar_telefone($nr_telefone){

	if(strlen($nr_telefone) == 11){ //00111115555 -> 00 11111 5555 -> (00) 11111-5555
		$pattern = '/(\d{2})(\d{5})(\d*)/';	
	}else if(strlen($nr_telefone) == 10){ //0011115555 -> 00 1111 5555 -> (00) 1111-5555
		$pattern = '/(\d{2})(\d{4})(\d*)/';
	}

	return preg_replace($pattern, '($1) $2-$3', $nr_telefone);
	
}
//FINAL FORMATAR NÚMERO TELEFONE #########################'

function limpar($string){

	$string = trim($string);
	$string =str_replace("'","",$string);//aqui retira aspas simples <'>
	$string =str_replace("\\","",$string);//aqui retira barra invertida<\\>
	$string =str_replace("UNION","",$string);//aqui retiro  o comando UNION <UNION>

	$banlist = array('"',"“","”", " insert", " select", " update", " delete", " distinct", " having", " truncate", "replace"," handler", " like", " as ", "or ", "procedure ", " limit", "order by", "group by", " asc", " desc","'","union all", "=", "'", "(", ")", "<", ">", " update", "-shutdown",  "--", "'", "#", "$", "%", "¨", "&", "'or'1'='1'", "--", " insert", " drop", "xp_", "*", " and");

	if(preg_match("/[a-zA-Z0-9]+/", $string)){
		$string = trim(str_replace($banlist,'', $string));
	}

	return $string;

}

function validaEmail($email){
	$er = "/^(([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}){0,1}$/";
	if (preg_match($er, $email)){
		return true;
	} else {
		return false;
	}
}

function select($conn, $TABELA, $PARAM = false, $WHERE, $INNER = false, $GROUP_BY = false, $HAVING = false, $ORDER_BY = false, $DEBUG = false){
	//VERIFICA SE PARAM É FALSE'
	if($PARAM == false){
		$PARAM = "";
	}

	//VERIFICA SE INNER É FALSE'
	if($INNER == false){
		$INNER = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($GROUP_BY == false){
		$GROUP_BY = "";
	}
	//VERIFICA SE HAVING É FALSE'
	if ($HAVING == false){
		$HAVING = "";
	}

	//VERIFICA SE GROUP_BY É FALSE'
	if ($ORDER_BY == false){
		$ORDER_BY = "";
	}

		//MONTANDO QUERY'
	$QUERY = "SELECT * " . $PARAM . " FROM " . $TABELA . $INNER . $WHERE . $GROUP_BY.$HAVING.$ORDER_BY;

	if($DEBUG){
		die('###SELECT>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function update($conn, $TABELA, $SET, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'UPDATE ' . $TABELA . $SET .$WHERE;

	if($DEBUG){
		die('###UPDATE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function delete($conn, $TABELA, $WHERE, $DEBUG = false){

	//MONTANDO QUERY'
	$QUERY = 'DELETE FROM '. $TABELA . $WHERE;

	if($DEBUG){
		die('###DELETE>>> '.$QUERY);
	}

	return $conn->query($QUERY);
}

function insert($conn, $TABELA, $PARAM, $LAST_ID = FALSE, $DEBUG = false){

	$QUERY = 'INSERT INTO ' . $TABELA . $PARAM;

	if($DEBUG){
		die('###INSERT>>> '.$QUERY);
	}

	if($LAST_ID){
		$conn->query($QUERY);
		return $conn->lastInsertId($LAST_ID);
	}

	return $conn->query($QUERY);
}


function paginacao($total_paginas, $url){
	$ul = '<ul class="paginacao">';
	for($i=1; $i <= $total_paginas; $i++ ):
		if(isset($_GET['page']) AND $_GET['page'] == $i){
			$classe = 'class="pagina_atual"';
		}else{
			$classe = '';
		}
		$ul .= '<li '. $classe.'><a href="'.$url.'page='.$i.'">'.$i.'</a></li>';
		endfor;
		$ul .= '</ul>';

		return $ul;
	}
/*################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################*/

//'################ INICIO VALIDAÇAÕ PARAMETRO 
function validar_parametros($dados){

	$retorno['res'] = 'ok';

	if(!isset($dados['categoryId'])){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Parametro incorreto! [categoryId]';	
	}else{
		if($dados['categoryId'] == ''){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Selecione uma categoria!';
		}else if(!is_numeric($dados['categoryId'])){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Categoria tem que ser um número!';
		}
	}

	if(!isset($dados['keyword'])){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Parametro incorreto! [keyword]';	
	}

	if(!isset($dados['priceMin'])){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Parametro incorreto! [priceMin]';	
	}

	if(!isset($dados['priceMax'])){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Parametro incorreto! [priceMax]';	
	}

	if(!isset($dados['sort'])){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Parametro incorreto! [sort]';	
	}
	if($dados['priceMin'] == '' AND $dados['priceMax'] != ''){
		if($dados['priceMax'] <= $dados['priceMin'] != ''){
			$retorno['res'] = 'error';
			$retorno['msg'] = 'Valor Máximo deve ser MAIOR que preço mínimo!';
		}
	}

	if(isset($retorno['res']) && $retorno['res'] == 'error'){
		echo(json_encode($retorno));
		exit;
	}elseif ($retorno['res'] && $retorno['res'] == 'ok') {
		
		if($dados['keyword'] == ''){
			unset($dados['keyword']);
		}

		if(isset($dados['param'])){
			unset($dados['param']);
		}

		/*if($dados['priceMin'] != ''){
			//$dados['priceMin'] = replace_virgula_ponto($dados['priceMin']);
			$dados['priceMin'] = number_format((float)$dados['priceMin'], '2', ',', '.');
		}

		if($dados['priceMax'] != ''){
			//$dados['priceMax'] = replace_virgula_ponto($dados['priceMax']);
			$dados['priceMax'] = number_format((float)$dados['priceMax'], '2', ',', '.');
		}*/

		return $dados;
	}

}
//'VALIDAÇAÕ PARAMETRO  #########################'










//'################ INICIO USUARIO DOAÇÃO
function get_usuario_doacao($conn, $find = false, $HAVING = false, $id = false){
	
	$TABELA = "tb_usuario_doacao tud";
	$WHERE =" WHERE  tud.ic_status = '1' AND ttd.ic_status = '1'";
	$INNER = " INNER JOIN tb_tipo_doacao ttd ON tud.id_tipo_doacao = ttd.id_tipo_doacao INNER JOIN tb_opcao_doacao tod ON tud.id_opcao_doacao = tod.id_opcao_doacao "; 
	$GROUP_BY = " GROUP BY tud.id_usuario_doacao";

	if($find){
		$WHERE .= $find;
	}

	if($id){
		$WHERE .= $id;
	}

					//$conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return  select($conn, $TABELA, false, $WHERE, $INNER, $GROUP_BY, $HAVING, false, false);

}

function validacao_usuario_doacao($conn, $dados_post, $DEBUG = false){

	if($DEBUG){
		echo('<pre>'); 
		print_r($dados_post); 
		die(); 
	}

	$retorno['res'] = 'ok';

	if($dados_post['nm_usuario_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo nome!';
	}

	if($dados_post['email_usuario_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o campo e-mail!';	
	}

	if($dados_post['sg_estado'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha a sigla do estado!';
	}

	if($dados_post['nm_cidade'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha a cidade!';
	}

	if($dados_post['nm_bairro'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o bairro!';
	}

	if($dados_post['nm_endereco'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o endereço!';
	}

	if($dados_post['nr_endereco'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o número do endereço!';
	}

	if($dados_post['nm_complemento'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o complemento!';
	}

	if($dados_post['cep'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o CEP!';
	}

	if($dados_post['nr_telefone'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o telefone!';
	}

	if($dados_post['vl_doacao'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Preencha o valor da doação!';
	}

	if($dados_post['id_tipos_doacoes'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione um tipo de doação!';
	}

	if($dados_post['id_opcoes_doacoes'] == ''){
		$retorno['res'] = 'error';
		$retorno['msg'] = 'Selecione uma opção de doação!';
	}

	if(isset($retorno['res']) && $retorno['res'] == 'error'){
		echo(json_encode($retorno));
		exit;
	}

}

//'FINAL USUARIO DOAÇÃO #########################'


//'################ INICIO TIPOS DOAÇÃO
function get_tipos_doacao($conn, $get_qtd = false){

	$TABELA = "tb_tipo_doacao ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_tipo_doacao ASC";

	if($get_qtd){
		$WHERE .= " AND id_tipo_doacao = ".$get_qtd;
	}
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL TIPOS DOAÇÃO #########################'

//'################ INICIO OPÇÕES DE DOAÇÃO

function get_opcoes_doacoes($conn){

	$TABELA = "tb_opcao_doacao ";
	$WHERE = " WHERE ic_status = '1' ";
	$ORDER_BY = " ORDER BY nm_opcao_doacao ASC";
							//conn, TABELA, PARAM, WHERE, INNER, GROUP_BY, HAVING, ORDER_BY, DEBUG
	return select($conn, $TABELA, false, $WHERE, false, false, false, $ORDER_BY, FALSE);

}
//'FINAL OPÇÕES DE DOAÇÃO #########################'

/*INICIO DEFINED PARA O BUSCAPE */
//INICIO SANDBOX
define('API_TOKEN_SANDBOX', '7PySlHysG3Zi');
define('URL_SANDBOX', 'http://sandbox.buscape.com.br/service/findOfferList/'.API_TOKEN_SANDBOX.'/BR/');
//FINAL SANDBOX

//INICIO PRODUÇÃO
define('API_TOKEN', '');
define('URL', 'http://bws.buscape.com.br/service/findOfferList/'.API_TOKEN.'/BR/');
//FINAL PRODUÇÃO
/*FINAL DEFINED PARA O BUSCAPE */


//'################ INICIO BUSCAPE
function get_offert_list($sandbox = false){

		//$data['email'] = 'comercioexpress@kbrtec.com.br';

	if($sandbox){
		$url_buscape = URL_SANDBOX;
	}else{
		$url_buscape = URL;
	}


	//para converter o array no formato correto
		//$data = http_build_query($data);

	//Inicia o cURL
	$curl = curl_init($url_buscape);
	//A URL a ser enviado os dados contem um certificado de segurança, nesse caso vamos ignorá-lo utilizando o parâmetro 
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	//O PagSeguro deverá responder enviando alguma coisa, vamos avisar ao cURL que precisaremos desses dados
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	//Vamos avisar ao cURL que esses dados será enviado via POST
	curl_setopt($curl, CURLOPT_POST, false);
	//PagSeguro só irá aceitar a versão 1.1 do HTTP
	curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	//iremos informar os dados que o cURL irá transportar.
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	//Com tudo pronto é hora de executar o cURL e gravar a resposta do PagSeguro 
	$xml = curl_exec($curl);
	//Se os dados de autenticação (e-mail e token) estiverem incorretos o PagSeguro irá exibir o texto: Unauthorized


	//liberar memoria deveremos fechar o cURL
	curl_close($curl);

	//para converter a resposta em um objeto
	$xml = simplexml_load_string($xml);


	//echo('<pre>'); print_r($xml); die();
	return $xml;
	//verificar o PagSeguro enviou algum erro
	/*if(count($xml->error) > 0){
		
		echo('<pre>');
		print_r($xml->error);
		echo('</pre>');
		
		exit;
	}

	return $url_checkout . $xml->code;*/
}
//'FINAL PAGAMENTO PAGSEGURO #########################'


/*'################################################################################################'
'#################################### FINAL FUNÇÕES DO SISTEMA ####################################'
'##################################################################################################*/
?>