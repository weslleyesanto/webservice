<?php 

$categorias = $buscape->topCategories(); 

if(isset($_GET)){

  $categoryId = '';
  $keyword = '';
  $priceMin = '';
  $priceMax = '';
  $sort = '';
  $page = 1;

  if(isset($_GET['param']) AND $_GET['param'] === 'q'){
    if(isset($_GET['categoryId']) and $_GET['categoryId'] != ""){
      $categoryId =  $_GET['categoryId'];
    }

    if(isset($_GET['keyword']) and $_GET['keyword'] != ""){
      $keyword = $_GET['keyword'];
    }
    if(isset($_GET['priceMin']) and $_GET['priceMin'] != ""){
      $priceMin = number_format($_GET['priceMin'], '2', ',', '.');
    }

    if(isset($_GET['priceMax']) and $_GET['priceMax'] != ""){
      $priceMax = number_format($_GET['priceMax'], '2', ',', '.');
    }
    if(isset($_GET['sort']) and $_GET['sort'] != ""){
      $sort = $_GET['sort'];
    }

    if(isset($_GET['page']) and $_GET['page'] != ""){
      $page = $_GET['page'];
    }

  }
}
//FINAL VERTICA _GET


?>
<div id="alert" style="display:none;"> </div>
<form  action="resultado.php" method="get" name="form_buscar" id="form_buscar">
  <fieldset>
    <legend>Filtro de busca</legend>
    <label>Categoria:</label>
    <select id="categoryId" name="categoryId" required="required">    
     <option value="">Selecione uma categoria</option>
     <?php  if(count($categorias) > 0): ?>
      <?php foreach ($categorias->subCategory as $key => $value): 
      $id_categoria = $value['id'];
      $nome_categoria = $value->name;

      if(isset($categoryId)){
        $selected = $id_categoria == $categoryId ? 'selected="selected"' : '';
      }
      ?>
      <option value="<?=$id_categoria?>" <?=$selected?>><?=$nome_categoria?></option>
    <?php endforeach;
    endif; ?>
  </select>
  <br/><br/>
  <label>Palavra chave:</label>
  <input type="text" placeholder="Ex: Notebook" id="keyword" name="keyword" value="<?=$keyword?>" />
  <br/><br/>
  <label>Faixa de preço:</label>
  <br/><br/>
  <label>Valor Mínimo: </label>
  <input type="text" placeholder="Ex: R$ 1.000,00" id="priceMin" name="priceMin" value="<?=$priceMin?>" />
  <label>Valor Máximo: </label>
  <input type="text" placeholder="Ex: R$ 1.300,00" id="priceMax" name="priceMax" value="<?=$priceMax?>"/>
  <br/><br/>
  <label>Ordenar por: </label>
    <select id="sort" name="sort">
      <option value="">Selecione uma ordenação</option>
      <option value="price" <?= $sort == 'price'  ? 'selected="selected"' : ''?>>Menor preço</option>
      <option value="dprice" <?= $sort == 'dprice'  ? 'selected="selected"' : ''?>>Maior preço</option>
      <option value="rate" <?= $sort == 'rate'  ? 'selected="selected"' : ''?>>Maior avaliação</option>
      <option value="drate" <?= $sort == 'drate'  ? 'selected="selected"' : ''?>>Menor avaliação</option>
    </select>
    <br/><br/>
    <input type="hidden" placeholder="" id="page" name="page" value="<?=$page?>"/>
    <input type="submit" name="Buscar" id="btnBuscar" value="Buscar"/>
  </fieldset>
</form>