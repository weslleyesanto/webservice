<?php include_once('include\config.php');



if(isset($_GET['page'])){
  $args = array('page' => $_GET['page']);
}else{
  $_GET['page'] = 1;
  $args = array('page' => $_GET['page']);
}

$retorno_ofertas = $buscape->topOffers($args);

$total_paginas = $retorno_ofertas['totalPages'];

$url = 'index.php?';

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Top Ofertas</title>
  <?php include_once('include\css.php'); ?>
</head>

<body>

  <div id="wrapper">

   <?php include_once('include\menu.php'); ?>

   <div id="page-wrapper">


    <div class="container-fluid">

      <?php include_once('include\filtro.php'); ?>

      <h1>Top Ofertas</h1>
      

      <div id="">

       <?php  if(count($retorno_ofertas) > 0) { ?>

       <ul class="bp-product-list">

        <li><?= paginacao($total_paginas, $url); ?></li>

        <?php foreach ($retorno_ofertas->offer as $key => $value): 

        $categoria_id = $value['categoryId'];
        $produto_id = $value['productId'];
        $id = $value['id'];
        $nome_oferta = $value->offerName;
        $link_img_oferta = $value->thumbnail['url'];
        $link = $value->links->link['url'];
        $de = number_format((float)$value->priceValue, 2, ',', '.');
        $ate = number_format((float)$value->priceFromValue, 2, ',', '.');
        $link_img_seller = $value->seller->thumbnail['url'];
        $nome_seller = $value->seller->sellerName;
        $id_saller = $value->seller['id'];
        
        $nome_eBitRating = $value->seller->rating->eBitRating->rating;
        $link_img_brand = $value->seller->rating->eBitRating->brand;
        
        $count_coments_loja = number_format((float)$value->seller->rating->userAverageRating->numComments, 0, '', '.');

        ?>
        <li data-id="<?=$produto_id?>" log-ofr-attribute="log-ofr-attribute" class="product track_checkout_container offer ocb-zup-lomadee" log_id="<?=$produto_id?>">
          <div class="image">
            <a data-preco="<?=$de?>" title="<?=$nome_oferta?>" class="product-image e7Click track_checkout"   href="#">
              <img class="product-image bp_prevent_error resize100" alt="<?=$nome_oferta?>" width="100" src="<?=$link_img_oferta?>" style="visibility: visible;">
            </a>
          </div>
          <div class="description">
            <a data-preco="<?=$de?>" class="e7Click track_checkout" href="#"><?=$nome_oferta?></a>
            <a class="offer-page" title="Mais informações" href="#">(+ info)</a>
          </div>
          <div class="details">
            <a data-preco="<?=$de?>" class="logo e7Click track_checkout" title="<?=$nome_seller?>" href="#">
              <img alt="<?=$nome_seller?>" src="<?=$link_img_seller?>">
            </a>
            <div class="store-info">
              <ul>
                <li class="">
                  <ul data-idebit="645" class="ebit-appear show-ebit">
                    <li>
                      <img alt="<?=$nome_seller?>" src="<?=$link_img_brand?>">
                    </li>
                  </ul>
                  <a href="avaliacao.php?param=q&productId=<?=$produto_id?>">
                    <span class="ratings"><?=$count_coments_loja?> compraram e avaliaram</span>
                  </a>
                  <div data-idebit="645" class="ebit-more-info loading">
                  </div>
                </li>
                <li class="store-details">
                  <a target="_blank" href="http://www.buscape.com.br/empresa/avaliacao-sobre-<?=replace_vazio($nome_seller)?>--<?=$id_saller?>.html">Mais detalhes da loja</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="content-holder">
            <div class="single-price">
              <strong class="title">Preço:</strong>
              <strong class="title-por hide">Por:</strong>
              <a data-preco="<?=$de?>" href="#" class="price e7Click track_checkout ">
                <span class="value">R$ <?= $de ?></span>
              </a>
            </div>
          </div>
        </li>
      <?php endforeach; ?>
      <li><?= paginacao($total_paginas, $url); ?></li>
      <?php }else{ ?>
      <li> Nenhuma oferta encontrada!</li>
      <?php }?>

    </ul>
  </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include_once('include\js.php'); ?>
<script src="assets/js/filtro.js"></script>
</body>

</html>
